#ifndef NCFN_H    // To make sure you don't declare the function more than once by including the header multiple times.
#define NCFN_H

#include <iostream>
#include <vector>
#include <tuple>
#include <limits>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include <math.h>
#include <bitset>


std::vector<std::vector<int>> checkPackages(std::vector<std::vector<int>> ffGraph, std::vector<int> destNodes, std::vector<int> codNodes, int flow);

std::vector<int> intersectV(std::vector<int> A, std::vector<int> B);

int getElemPosV(std::vector<int> V, int elem);

std::vector<int> unionV(std::vector<int> A, std::vector<int> B);

#endif
