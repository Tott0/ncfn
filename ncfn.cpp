#include "ncfn.h"
using namespace std;

// TODO start mat with all the packages, so there's always intersections.


vector<int> intersectV(vector<int> A, vector<int> B){
	vector<int> inters;
	for(int i = 0; i < B.size(); i++){
		if(find(A.begin(), A.end(), B.at(i)) != A.end()){
			inters.push_back(B.at(i));
		}
	}
	return inters;
}

int getElemPosV(vector<int> V, int elem){
	return (find(V.begin(), V.end(), elem)) - V.begin();
}


vector<vector<int>> ffGraphP;
vector<int> destNodesP;
vector<int> codNodesP;
vector<vector<int>>   distrib; //matriz principal
vector<vector<int>> packages;  //distribución de paquetes
bool error = false;            //control de validez de la solución
int flowP;
int nonCodNodes;
vector<int> validPackages;

vector<int> exploreSNodes(int origen, bool cod){
	vector<int> exploreQ;
	vector<int> endPoints;
	if(cod){
		for(int i = 0; i < ffGraphP.size(); i++){
			if(ffGraphP[origen][i] == 1){
				exploreQ.push_back(i);
			}
		}
	}else{
		exploreQ.push_back(origen);
	}


	while(!exploreQ.empty()){
		int node = exploreQ.at(0);
		exploreQ.erase(exploreQ.begin());
		int isDes = getElemPosV(destNodesP, node);
		int isCod = getElemPosV(codNodesP, node);
		if(isDes != destNodesP.size() || isCod != codNodesP.size()){
			endPoints.push_back(node);
		}else{
			for(int i = 0; i < ffGraphP.size(); i++){
				if(ffGraphP.at(node).at(i) == 1){
					exploreQ.push_back(i);
				}
			}
		}
	}
	return endPoints;
}

vector<int> unionV(vector<int> A, vector<int> B){
		for(int i = 0; i < B.size(); i++){
			if(getElemPosV(A, B.at(i)) == A.size()){
				A.push_back(B.at(i));
			}
		}
		return A;
}

int NumberOfSetBits(int i)
{
     i = i - ((i >> 1) & 0x55555555);
     i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
     return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
}

vector<int> getValidPackages(int flow){
  vector<int> validPackages = vector<int>();
  vector<vector<int>> allPkts = vector<vector<int>>(flow, vector<int>());
  for(int i = 1; i < (int)pow(2, flow); i++){
    int b = NumberOfSetBits(i);
    allPkts.at(b - 1).push_back(i);
  }

  for(int i = 0; i < flow; i++){
    validPackages.insert(validPackages.end(), allPkts.at(i).begin(), allPkts.at(i).end());
  }

  return validPackages;
}

vector<int> getOppositePackagesV(vector<int> p){
	vector<int> opp;
	vector<int> copy = validPackages;
	for(int i = 0; i < p.size(); i++){
    int flag = p.at(i);
		int a = opp.size();
    opp.push_back(flag);
		copy.erase(copy.begin() + getElemPosV(copy, flag));

    for(int j = 0; j < a; j++){
      int comb = flag ^ opp.at(j);
      opp.push_back(comb);
      copy.erase(copy.begin() + getElemPosV(copy, comb));
    }
	}
	return copy;

}


void checkCodCol(int colC);

int getParentNumber(int cod){
	int p = 0;
	for(int i = 0; i < ffGraphP.size(); i++){
		if(ffGraphP[i][cod] == 1){
			p++;
		}
	}
	return p;
}

int findLastParent(int cod){
	int i = 0;
	while( i < distrib.size()){
		if(packages[i].size() != 1){
			if(getElemPosV(distrib[i], cod) != distrib[i].size()){
				return i;
			}
		}
		i++;
	}
	return i;
}

vector<int> getCurrentPkts(int node){
	vector<int> p;
	for(int i = 0; i < distrib.size(); i++){
		if(packages.at(i).size() == 1){
			if(getElemPosV(distrib.at(i), node) != distrib.at(i).size()){
				p.push_back(packages.at(i).at(0)); //si hay una columna con paquetes fijos que tenga al cod agrega el pkt.
			}
		}
	}
	return p;
}

void checkCodNm1(int colC);

void setCombOpp(int col, vector<int> p){
	p = intersectV(p, validPackages);
	//###
	cout << "pkts: ";
	for(int j = 0; j < p.size(); j++){
		cout << p[j] << " ";
	}
	cout << endl;
	//###
	int init = packages.at(col).size();
	if(packages.at(col).empty()){
		packages.at(col) = p;
	}else{
		packages.at(col) = intersectV(packages.at(col), p);
	}
	if(packages.at(col).empty()){
		error = true;
	}
	int end = packages.at(col).size();
	//si i es cod verificar
	if(col >= nonCodNodes && init != end){
		if(packages.at(col).size() == 1){
			cout << "col " << col << "set, checking..." << endl;
			checkCodCol(col);
		}else{
			int colSet = getCurrentPkts(codNodesP[col - nonCodNodes]).size();
			if(packages.at(col).size() > 1 && colSet == getParentNumber(codNodesP[col - nonCodNodes]) - 1){
				checkCodNm1(col);
			}
		}
	}
}

void checkCodNm1(int colC){
	cout << "cod " << colC << "n-1 sets, checking..." << endl;

	int colL = findLastParent(codNodesP[colC - nonCodNodes]);
	cout << "last parent  " << colL << endl;
	if(colL == distrib.size()){
		cout << "parents set" << endl;
	}else{
		vector<int> p = getCurrentPkts(codNodesP[colC - nonCodNodes]);
		int comb = p[0];
		for(int i = 1; i < p.size(); i++){
			comb = comb ^ p[i];
		}

		p = vector<int>();
		for(int i = 0; i < packages.at(colC).size(); i++){
			int combT = comb ^ packages[colC][i];
			if(combT != 0){
				p.push_back(combT);
			}
		}
		setCombOpp(colL, p);
	}

	for(int i = 0; i < distrib.at(colC).size(); i++){
		colL = getElemPosV(codNodesP, distrib.at(colC).at(i));
		if(colL != codNodesP.size()){
			colL += nonCodNodes;
			int colSet = getCurrentPkts(codNodesP[colL - nonCodNodes]).size();
			if(colSet == getParentNumber(codNodesP[colL - nonCodNodes]) - 1){
				cout << "child cod " << colL << endl;

				vector<int> p = getCurrentPkts(codNodesP[colL - nonCodNodes]);
				int comb = p[0];
				for(int i = 1; i < p.size(); i++){
					comb = comb ^ p[i];
				}

				p = vector<int>();
				for(int i = 0; i < packages.at(colC).size(); i++){
					int combT = comb ^ packages[colC][i];
					if(combT != 0){
						p.push_back(combT);
					}
				}
				setCombOpp(colL, p);
			}
		}
	}
}



void setCodCol(int colC){
	int cod = codNodesP[colC - nonCodNodes];

	int colSet = getCurrentPkts(cod).size();
	int parentNum = getParentNumber(cod);

	vector<int> p = getCurrentPkts(cod);
	if(colSet == parentNum){
		int comb = p[0];
		for(int i = 1; i < p.size(); i++){
			comb = comb ^ p[i];
		}
		p = vector<int>(1, comb);
	}else{
		p = getOppositePackagesV(p);
	}
	//###
	cout << "pkts: ";
	for(int j = 0; j < p.size(); j++){
		cout << p[j] << " ";
	}
	cout << endl;
	//###
	int init = packages.at(colC).size();
	if(packages.at(colC).empty()){
		packages.at(colC) = p;
	}else{
		packages.at(colC) = intersectV(packages.at(colC), p);
	}
	if(packages.at(colC).empty()){
		error = true;
	}
	int end = packages.at(colC).size();
	if(init != end){
		if( colSet == parentNum){
			cout << "col " << colC << "set, checking..." << endl;
			checkCodCol(colC);
		}else{
			if(colSet == parentNum - 1){
				checkCodNm1(colC); //TODO is this necessary
			}
		}


	}

}

int countColumns(int node){
	int p = 0;
	for(int i = 0; i < distrib.size(); i++){
		if(getElemPosV(distrib.at(i), node) != distrib.at(i).size()){
			p++;
		}
	}
	return p;
}

void intersectColumns(int col){
	int n = 0;
	while(n < distrib.at(col).size() && !error){
    int node = distrib.at(col).at(n);
    cout << "intersecting node " << node << endl;
		vector<int> p = getCurrentPkts(node);
		if(p.size() != countColumns(node)){
			p = getOppositePackagesV(p);
			//###
			cout << "pkts: ";
			for(int j = 0; j < p.size(); j++){
				cout << p[j] << " ";
			}
			cout << endl;
			//###

			for(int i = 0; i < distrib.size(); i++){
	      if(packages.at(i).size() != 1){
	        if(getElemPosV(distrib.at(i), node) != distrib.at(i).size()){
	          cout << "intersect with " << i << endl;
						int init = packages.at(i).size();
	      		if(packages.at(i).empty()){
	      			packages.at(i) = p;
	      		}else{
	      			packages.at(i) = intersectV(packages.at(i), p);
	      		}
	      		if(packages.at(i).empty()){
	      			error = true;
	      		}
	          int end = packages.at(i).size();
	      		//si i es cod verificar
	      		if(i >= nonCodNodes && init != end){
							int colSet = getCurrentPkts(codNodesP[i - nonCodNodes]).size();
							if(colSet == getParentNumber(codNodesP[i - nonCodNodes]) - 1){
								checkCodNm1(i);
							}

	      			if(packages.at(i).size() == 1){
	              cout << "col " << i << "set, checking..." << endl;
	      				checkCodCol(i);
	      			}
	      		}
	        }
	      }
	    }
		}
    n = n + 1;
	}
}

void checkCodCol(int colC){
	for(int i = 0; i < distrib.at(colC).size(); i++){
		int codPos = getElemPosV(codNodesP, distrib.at(colC).at(i));
		if(codPos != codNodesP.size()){
			codPos += nonCodNodes;
      cout << "cod found " << codPos << endl;
			setCodCol(codPos);
		}
	}
	intersectColumns(colC);
}

vector<vector<int>> checkPackages(vector<vector<int>> ffGraph, vector<int> destNodes, vector<int> codNodes, int flow){
	ffGraphP = ffGraph;
	destNodesP = destNodes;
	codNodesP = codNodes;
	distrib = vector<vector<int>>();
	error = false;
	flowP = flow;
  validPackages = getValidPackages(flow);

  //#####
  cout << "validPackages:" << endl;
  for(int i = 0; i < validPackages.size(); i++){
    cout << validPackages.at(i) << "  ";
  }
  cout << endl << endl;
  //#####

	//agregamos los S->N
	for(int i = 0; i < ffGraphP.size(); i++){
		if(ffGraphP.at(0).at(i) == 1){
			//explora descendentemente, se detiene en destino o codificador
			vector<int> temp = exploreSNodes(i, false);
			distrib.push_back(temp);
		}
	}

	//agregamos los codificadores
	nonCodNodes = distrib.size(); // separación entre nodos origen | codificadores
	for(int i = 0; i < codNodesP.size(); i++){
		vector<int> temp = exploreSNodes(codNodesP.at(i), true);
		distrib.push_back(temp);
	}

	//###Distrib
	cout << "------ distrib -------" << endl;
	for(int i = 0; i < distrib.size(); i++){
		cout << i << ": ";
		for(int j = 0; j < distrib.at(i).size(); j++){
			cout << distrib[i][j] << " ";
		}
		cout << endl;
	}
	cout << "------ distrib -------" << endl;
	//###

	packages = vector<vector<int>>(distrib.size(), vector<int>()); //paquetes se inicializa con un tamaño = distrib
  int cNum = 0;
	while(cNum < nonCodNodes && !error){
    cout << endl << "checking " << cNum << endl;
		//Se fija un paquete en la columna en revisión
		if(packages.at(cNum).empty()){
			packages.at(cNum).push_back(01); //si no esta marcada se marca con 01
		}else{
			if(packages.at(cNum).size() > 1){
				packages.at(cNum).erase(packages.at(cNum).begin() + 1, packages.at(cNum).end());
			}
		}

		//###Packages
		cout << endl << "------ packages -------" << endl;
		for(int i = 0; i < packages.size(); i++){
			cout << i << ": ";
			for(int j = 0; j < packages.at(i).size(); j++){
				cout << packages[i][j] << " ";
			}
			cout << endl;
		}
		cout << "------ packages -------" << endl << endl;
		//###

		//para cada codificador en cNum, marca con los opuestos
		for(int i = 0; i < distrib.at(cNum).size(); i++){
			int codPos = getElemPosV(codNodesP, distrib.at(cNum).at(i));
			if(codPos != codNodesP.size()){
				codPos += nonCodNodes;
        cout << "cod found " << codPos << endl;
				setCodCol(codPos);
			}
		}
		intersectColumns(cNum);

    cNum++;
	}
	cout << endl <<" --- DONE --- "<< endl;
	return packages;
}

// int main()
// {
// 	string line;
// 	string matName = "38NodesFlow3_opt";
// 	ifstream Capacity("Inputs/"+matName+".ffm");
//   int flow;
//   vector<vector<int>> ffGraph;
// 	vector<int> codNodes;
//   vector<int> destNodes;
//
// 	if(Capacity.is_open()){
//     getline(Capacity, line);
// 		flow = stoi(line);
// 		getline(Capacity, line);
// 		stringstream ss(line);
// 		string item;
// 		while(getline(ss, item, ' ')){
// 			destNodes.push_back(stoi(item));
// 		}
//
//     getline(Capacity, line);
// 		stringstream ss2(line);
// 		string item2;
// 		while(getline(ss2, item2, ' ')){
// 			codNodes.push_back(stoi(item2));
// 		}
//
// 		while(getline(Capacity, line)){
// 			vector<int> col;
// 			stringstream ss(line);
// 			string item;
// 			while(getline(ss, item, ' ')){
// 				col.push_back(stoi(item));
// 			}
// 			ffGraph.push_back(col);
// 		}
// 		Capacity.close();
// 	}else{
// 		cout << "Unable to open file\n";
// 	}
//   vector<vector<int>> packages = checkPackages(ffGraph, destNodes, codNodes, flow);
//
// 	cout << endl << endl << "------- Solution ----------" << endl;
//   for(int i = 0; i < packages.size(); i++){
//     cout << bitset<8>(packages.at(i).at(0)) << endl;
//   }
// 	cout << endl << endl;
//   system("pause");
// }
