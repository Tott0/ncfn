

# README #

### gcc compiler cmd ###
g++ -std=c++11 ff_pf.cpp ncfn.cpp -o Output	

### NC_NS3 ###
./waf --run "network_coding_pf --ffInput=18NodesFluMax --pktNPair=1 --RngSeed=3"


### Vector Reference ###
* [Vector](http://www.cplusplus.com/reference/vector/vector/)

### Get C++ Compiler ###
* [Compiler](http://tdm-gcc.tdragon.net/)

### NS3 Multicast Routing Example ###
* [Multicast](https://www.nsnam.org/doxygen/csma-multicast_8cc_source.html)
